function eGMessageService (config) {
    var service = {
        config: {
            timeout: 1000,
            defaultMessage: 'Algum problema ocorreu.'
        }
    };

    if(config) {
        service.config = $.extend(service.config, config);
    }

    service.errorMessage = function(msg, timeout) {
        $.snackbar({ content : msg || service.config.defaultMessage, timeout: timeout || service.config.timeout });
    };

    service.successMessage = function(msg, timeout) {
        $.snackbar({ content : msg || service.config.defaultMessage, timeout: timeout || service.config.timeout });
    };

    return service;
}

angular.module("eGMessages", []).factory("eGMessageService", eGMessageService);
